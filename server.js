import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import cors from "cors";
import dotenv from "dotenv";

import fs from "fs";

dotenv.config({ path: ".env" });

const app = express();
const allowedOrigin = process.env.CORS;
app.use(
  cors({
    origin: (origin, callback) => {
      if (!origin) return callback(null, true);
      if (allowedOrigin.indexOf(origin) == -1) {
        var msg =
          "The Cors policy for this site does not" +
          "allow access from the specifiend Origin.";
        return callback(msg, false);
      }
      return callback(null, true);
    },
  })
);
app.use(helmet());

app.use(bodyParser.json({ extended: true, limit: "5mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "5mb" }));

app.get("/", (req, res) => {
  res.send("ok");
});

app.post("/register", (req, res) => {
  const path = `public/dataface/${req.body.label}`;

  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true });
  }

  fs.writeFile(
    `${path}/face_descriptions.json`,
    JSON.stringify(req.body.descriptors),
    "utf8",
    () => console.log("File Created")
  );
  res.send("ok");
});

app.get("/get", (req, res) => {
  const dir = fs.readdirSync("public/dataface", { withFileTypes: true });

  let response = [];

  for (let user of dir) {
    const descriptors = fs.readFileSync(
      `public/dataface/${user.name}/face_descriptions.json`,
      "utf8"
    );
    response.push({
      label: user.name,
      descriptors: JSON.parse(descriptors),
    });
  }

  console.log(dir);

  res.status(200).json(response);
});

app.listen(process.env.PORT, () => {
  console.log(`Example app listening on port ${process.env.PORT}`);
});
