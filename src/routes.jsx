import App from "./App";

export const mainRoutes = {
  path: "/",
  element: <App />,
  errorElement: <p>Page Not Found</p>,
  children: [
    {
      index: true,
      lazy: () => import("./pages/Home"),
    },
    {
      path: "/register",
      lazy: () => import("./pages/Register"),
    },
    {
      path: "/face-detections",
      lazy: () => import("./pages/FaceDetections"),
    },
  ],
};
