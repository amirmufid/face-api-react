import { Typography, Card } from "@material-tailwind/react";
import { Link, Outlet } from "react-router-dom";

export default function App() {
  return (
    <div className="relative grid min-h-[100vh] w-screen p-8">
      <div className="flex flex-col-reverse items-center justify-between gap-4 self-start md:flex-row">
        <Card className="h-max w-max border border-blue-gray-50 py-4 px-5 shadow-lg shadow-blue-gray-900/5">
          <code className="text-blue-gray-900">
            &copy; Copyright <strong>Pantona</strong>
          </code>
        </Card>
        <Card className="h-max w-max flex-row items-center border border-blue-gray-50 font-semibold text-blue-gray-900 shadow-lg shadow-blue-gray-900/5">
          <img src="/vite.svg" alt="Vite Logo" className="ml-4 h-7 w-7" />
          <div className="py-4 pl-4 pr-5">Vite Template</div>
        </Card>
      </div>
      <div className="flex gap-2 text-center justify-center">
        <Outlet />
      </div>
      <div className="grid grid-cols-1 gap-4 self-end md:grid-cols-2 lg:grid-cols-4">
        <Link to="register">
          <Card
            shadow={false}
            className="border border-blue-gray-50 py-4 px-5 shadow-xl shadow-transparent transition-all hover:-translate-y-4 hover:border-blue-gray-100/60 hover:shadow-blue-gray-900/5">
            <Typography
              variant="h5"
              color="blue-gray"
              className="mb-3 flex items-center gap-3">
              Register
            </Typography>
            <Typography color="blue-gray" className="font-normal opacity-70">
              Register FACE
            </Typography>
          </Card>
        </Link>
        <Link to="face-detections">
          <Card
            shadow={false}
            className="border border-blue-gray-50 py-4 px-5 shadow-xl shadow-transparent transition-all hover:-translate-y-4 hover:border-blue-gray-100/60 hover:shadow-blue-gray-900/5">
            <Typography
              variant="h5"
              color="blue-gray"
              className="mb-3 flex items-center gap-3">
              Face Recognition
            </Typography>
            <Typography color="blue-gray" className="font-normal opacity-70">
              Face Recognition
            </Typography>
          </Card>
        </Link>
      </div>
    </div>
  );
}
