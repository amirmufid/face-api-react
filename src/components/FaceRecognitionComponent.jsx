import Webcam from "react-webcam";
import { useRef, useEffect, useState } from "react"; // import useCallback
import "./WebcamComponent.css";
import * as faceapi from "face-api.js";
import * as tf from "@tensorflow/tfjs";
import { Alert } from "@material-tailwind/react";
const minConfidence = 0.5;
const fakeThreshold = 0.8;
const maxResults = 5;

const FaceRecoginitionComponent = ({ faceDescriptors, onScaned }) => {
  const webcamRef = useRef(null);
  const canvasRef = useRef();
  const [imgSrc, setImgSrc] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  const [faceRecognitionState, setFaceRecognitionState] = useState({
    cameraIsReady: false,
    modelsIsReady: false,
    faceMatcherIsReady: false,
    canvasIsReady: false,
    cameraDimension: {
      height: 0,
      width: 0,
    },
    faceDescriptions: null,
    submitedFaceDescriptions: [],
    submitedFaceImage: [],
    faceMatcher: null,
  });

  useEffect(() => {
    if (faceRecognitionState.cameraIsReady) {
      loadModels();
    }
  }, [faceRecognitionState.cameraIsReady]);

  useEffect(() => {
    if (faceRecognitionState.modelsIsReady) {
      initCanvas();
      initFaceMatcher();
    }
  }, [faceRecognitionState.modelsIsReady]);

  useEffect(() => {
    const initFaceDetector = async () => {
      console.log("FACE DETECTION");

      console.log("Loading Anti-Spoofing model");
      const antispoofing = await tf.loadGraphModel("models/anti-spoofing.json");
      const inputSize = Object.values(antispoofing.modelSignature["inputs"])[0]
        .tensorShape.dim[2].size;

      const detections = await faceapi
        .detectSingleFace(webcamRef.current.video)
        .withFaceLandmarks()
        .withFaceDescriptor();

      const box = [
        detections.alignedRect.box.x,
        detections.alignedRect.box.y,
        detections.alignedRect.box.width,
        detections.alignedRect.box.height,
      ].map((a) => Math.round(a));
      console.log(
        `detections: ${Math.round(
          100 * detections.detection.score
        )}% confidence`,
        "Box:",
        box
      );

      // .withFaceExpressions()

      canvasRef.current
        .getContext("2d", { willReadFrequently: true })
        .clearRect(
          0,
          0,
          webcamRef.current.video.clientWidth,
          webcamRef.current.video.clientHeight
        );
      if (detections) {
        const resizedDetections = faceapi.resizeResults(detections, {
          width: webcamRef.current.video.clientWidth,
          height: webcamRef.current.video.clientHeight,
        });

        if (faceRecognitionState.faceMatcher) {
          const result = faceRecognitionState.faceMatcher?.findBestMatch(
            resizedDetections.descriptor
          );

          if (result) {
            const box = resizedDetections.detection.box;
            const drawBox = new faceapi.draw.DrawBox(box, {
              label: result,
            });
            drawBox?.draw(canvasRef.current);

            handleScanned(result.label);
          } else {
            setTimeout(() => initFaceDetector(), 100);
          }
        } else {
          setTimeout(() => initFaceDetector(), 100);
        }
      }
    };

    if (
      faceRecognitionState.canvasIsReady &&
      faceRecognitionState.faceMatcherIsReady
    ) {
      console.log("FACE DB READY");
      initFaceDetector();
    }
  }, [
    faceRecognitionState.canvasIsReady,
    faceRecognitionState.faceMatcherIsReady,
  ]);

  const handleScanned = (label) => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImgSrc(imageSrc);
    onScaned(label);
  };

  const loadModels = () => {
    console.log(">>>>LOAD MODELS<<<<<");

    Promise.all([
      // THIS FOR FACE DETECT AND LOAD FROM YOU PUBLIC/MODELS DIRECTORY
      faceapi.tf.setBackend("cpu"),
      faceapi.tf.ready(),

      faceapi.nets.ssdMobilenetv1.loadFromUri("/models"),
      faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
      faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      faceapi.nets.faceExpressionNet.loadFromUri("/models"),
    ])
      .then(() => {
        console.log(">>>>MODELS READY<<<<<");
        setFaceRecognitionState((state) => {
          return {
            ...state,
            modelsIsReady: true,
          };
        });
      })
      .catch((err) => {
        console.log(err);
        setErrorMessage("Failed load Models");
        setFaceRecognitionState((state) => {
          return {
            ...state,
            modelsIsReady: false,
          };
        });
      });
  };

  const initCanvas = () => {
    console.log(">>>>LOAD CANVAS<<<<<");

    // DRAW YOU FACE IN WEBCAM
    canvasRef.current.innerHtml = faceapi.createCanvasFromMedia(
      webcamRef.current.video
    );

    faceapi.matchDimensions(canvasRef.current, {
      width: webcamRef.current.video.clientWidth,
      height: webcamRef.current.video.clientHeight,
    });

    console.log(">>>>CANVAS READY<<<<<");

    setFaceRecognitionState((state) => {
      return {
        ...state,
        canvasIsReady: true,
      };
    });
  };

  const getLabeledFaceDescriptions = () => {
    return Promise.all(
      faceDescriptors.map(async (item) => {
        const descriptions = [];
        item.descriptors.forEach((descriptior) => {
          let df32jsonArr = new Float32Array(JSON.parse(descriptior));
          descriptions.push(df32jsonArr);
        });
        return new faceapi.LabeledFaceDescriptors(item.label, descriptions);
      })
    );
  };

  const initFaceMatcher = async () => {
    const labeledFaceDescriptors = await getLabeledFaceDescriptions();

    if (labeledFaceDescriptors.length) {
      const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors);

      setFaceRecognitionState((state) => {
        return {
          ...state,
          faceMatcherIsReady: true,
          faceMatcher: faceMatcher,
        };
      });
    } else {
      setErrorMessage("No Data Face found !");
    }
  };

  return (
    <div className="container">
      {imgSrc ? (
        <img src={imgSrc} alt="webcam" />
      ) : (
        <>
          <Webcam
            ref={webcamRef}
            onUserMedia={() => {
              setFaceRecognitionState((state) => {
                return {
                  ...state,
                  cameraIsReady: true,
                };
              });
            }}
            onUserMediaError={(err) => {
              console.log("MEDIA ERROR", err);
            }}
          />
          {errorMessage && <Alert color="red">{errorMessage}</Alert>}
          <canvas ref={canvasRef} className="appcanvas" />
        </>
      )}
    </div>
  );
};

export default FaceRecoginitionComponent;
