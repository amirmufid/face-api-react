import Webcam from "react-webcam";
import { useRef, useEffect, useState } from "react"; // import useCallback
import "./WebcamComponent.css";
import * as faceapi from "face-api.js";

const WebcamComponent = ({ btnCaptureRef, handleSubmit }) => {
  const webcamRef = useRef(null);
  const canvasRef = useRef();

  const [faceRecognitionState, setFaceRecognitionState] = useState({
    cameraIsReady: false,
    modelsIsReady: false,
    faceMatcherIsReady: false,
    canvasIsReady: false,
    cameraDimension: {
      height: 0,
      width: 0,
    },
    faceDescriptions: null,
    submitedFaceDescriptions: [],
    submitedFaceImage: [],
    faceMatcher: null,
  });

  useEffect(() => {
    if (faceRecognitionState.cameraIsReady) {
      loadModels();
    }
  }, [faceRecognitionState.cameraIsReady]);

  useEffect(() => {
    if (faceRecognitionState.modelsIsReady) {
      initCanvas();
    }
  }, [faceRecognitionState.modelsIsReady]);

  useEffect(() => {
    const initFaceDetector = async () => {
      // const faceDetectionNet = faceapi.nets.ssdMobilenetv1;
      // // export const faceDetectionNet = tinyFaceDetector

      // // SsdMobilenetv1Options
      // const minConfidence = 0.5;

      // // TinyFaceDetectorOptions
      // const inputSize = 408;
      // const scoreThreshold = 0.5;

      // function getFaceDetectorOptions(net) {
      //   return net === faceapi.nets.ssdMobilenetv1
      //     ? new faceapi.SsdMobilenetv1Options({ minConfidence })
      //     : new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold });
      // }

      // const faceDetectionOptions = getFaceDetectorOptions(faceDetectionNet);

      if (webcamRef.current?.video) {
        const detections = await faceapi
          .detectSingleFace(webcamRef.current.video)
          .withFaceLandmarks()
          .withFaceDescriptor();

        // .withFaceExpressions()

        canvasRef.current
          .getContext("2d", { willReadFrequently: true })
          .clearRect(
            0,
            0,
            webcamRef.current.video.clientWidth,
            webcamRef.current.video.clientHeight
          );
        if (detections) {
          setFaceRecognitionState((state) => {
            return {
              ...state,
              faceDescriptions: detections.descriptor,
            };
          });
          const resizedDetections = faceapi.resizeResults(detections, {
            width: webcamRef.current.video.clientWidth,
            height: webcamRef.current.video.clientHeight,
          });

          // const results = resizedDetections.map((d) => {
          //   // console.log(d.descriptor)
          //   return faceRecognitionState.faceMatcher.findBestMatch(d.descriptor);
          // });
          // results.forEach((result, i) => {
          //   const box = resizedDetections[i].detection.box;
          //   const drawBox = new faceapi.draw.DrawBox(box, {
          //     label: result,
          //   });
          //   drawBox?.draw(canvasRef.current);
          // });
          faceapi.draw.drawDetections(canvasRef.current, resizedDetections);
          // faceapi.draw.drawFaceLandmarks(canvasRef.current, resizedDetections);
          // faceapi.draw.drawFaceExpressions(canvasRef.current,resizedDetections)
        }
        setTimeout(() => initFaceDetector(), 100);
      }
    };
    if (faceRecognitionState.canvasIsReady) {
      initFaceDetector();
    }
  }, [faceRecognitionState.canvasIsReady]);

  useEffect(() => {
    const btn = btnCaptureRef.current;
    btn.addEventListener("click", handleClick);

    return () => btn.removeEventListener("click", handleClick);
  }, [faceRecognitionState]);

  const handleClick = () => {
    // const imageSrc = webcamRef.current.getScreenshot();
    const submitedFaceDescriptions = JSON.stringify(
      Array.from(faceRecognitionState.faceDescriptions)
    );
    handleSubmit(submitedFaceDescriptions);
  };
  const loadModels = () => {
    console.log(">>>>LOAD MODELS<<<<<");
    Promise.all([
      // THIS FOR FACE DETECT AND LOAD FROM YOU PUBLIC/MODELS DIRECTORY
      faceapi.nets.ssdMobilenetv1.loadFromUri("/models"),
      faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
      faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
      faceapi.nets.faceExpressionNet.loadFromUri("/models"),
    ])
      .then(() => {
        console.log(">>>>MODELS READY<<<<<");
        setFaceRecognitionState((state) => {
          return {
            ...state,
            modelsIsReady: true,
          };
        });
      })
      .catch((err) => {
        console.log(err);
        setFaceRecognitionState((state) => {
          return {
            ...state,
            modelsIsReady: false,
          };
        });
      });
  };

  const initCanvas = () => {
    console.log(">>>>LOAD CANVAS<<<<<");

    // DRAW YOU FACE IN WEBCAM
    canvasRef.current.innerHtml = faceapi.createCanvasFromMedia(
      webcamRef.current.video
    );

    faceapi.matchDimensions(canvasRef.current, {
      width: webcamRef.current.video.clientWidth,
      height: webcamRef.current.video.clientHeight,
    });

    console.log(">>>>CANVAS READY<<<<<");

    setFaceRecognitionState((state) => {
      return {
        ...state,
        canvasIsReady: true,
      };
    });
  };

  return (
    <div className="container">
      <Webcam
        ref={webcamRef}
        onUserMedia={() => {
          setFaceRecognitionState((state) => {
            return {
              ...state,
              cameraIsReady: true,
            };
          });
        }}
        onUserMediaError={(err) => {
          console.log("MEDIA ERROR", err);
        }}
      />
      <canvas ref={canvasRef} className="appcanvas" />
    </div>
  );
};

export default WebcamComponent;
