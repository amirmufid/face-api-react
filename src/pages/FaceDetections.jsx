import axios from "redaxios";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
} from "@material-tailwind/react";
import FaceRecoginitionComponent from "../components/FaceRecognitionComponent";
import { useEffect, useState } from "react";
const API = axios.create({ baseURL: import.meta.env.VITE_API_URL });
const FaceDetections = () => {
  const [faceDescriptors, setFaceDescriptors] = useState(null);
  const [userDetected, setUserDetected] = useState(null);

  useEffect(() => {
    API.get("/get").then((res) => {
      setFaceDescriptors(res.data);
    });
  }, []);

  return (
    <Card className="w-96">
      <CardHeader
        shadow={false}
        floated={false}
        variant="gradient"
        color="purple"
        className="grid  m-0 place-items-center">
        {faceDescriptors ? (
          <FaceRecoginitionComponent
            faceDescriptors={faceDescriptors}
            onScaned={setUserDetected}
          />
        ) : (
          "Initialize Face DB"
        )}
      </CardHeader>
      <CardBody>
        <Typography
          variant="small"
          color="gray"
          className="font-normal opacity-75">
          Scan wajah sampai terdeteksi
        </Typography>
      </CardBody>
      <CardFooter className="pt-0">
        <Typography variant="h2" color="blue-gray" className="font-medium">
          Welcome
        </Typography>
        <Typography variant="h1">
          {userDetected ? `"${userDetected}"` : ""}
        </Typography>
      </CardFooter>
    </Card>
  );
};

export const Component = FaceDetections;
