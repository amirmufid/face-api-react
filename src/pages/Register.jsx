import axios from "redaxios";
import { useRef, useState } from "react";
import WebcamComponent from "../components/WebcamComponent";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Button,
  Stepper,
  Step,
  Input,
} from "@material-tailwind/react";

const API = axios.create({ baseURL: import.meta.env.VITE_API_URL });

const Register = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [username, setUsername] = useState("");
  const btnCaptureRef = useRef(null);

  const [submitedFaceDescriptions, setSubmitedFaceDescriptions] = useState([]);

  const handleSubmit = (faceDescription) => {
    console.log(activeStep, "activeStep");
    if (activeStep < 4) {
      setActiveStep((state) => state + 1);
      setSubmitedFaceDescriptions((state) => [...state, faceDescription]);
    } else {
      setActiveStep((state) => state + 1);
      console.log("SUBMIT", submitedFaceDescriptions);
      API.post("/register", {
        label: username,
        descriptors: submitedFaceDescriptions,
      });
    }
  };

  return (
    <Card className="w-96">
      <CardHeader
        shadow={false}
        floated={false}
        variant="gradient"
        color="purple"
        className="grid  m-0 place-items-center">
        {activeStep < 5 ? (
          <WebcamComponent
            btnCaptureRef={btnCaptureRef}
            handleSubmit={handleSubmit}
          />
        ) : (
          <Typography variant="h1" color="inherit">
            COMPLETED
          </Typography>
        )}
        <div className="w-full px-20 pt-4 pb-8 mb-4">
          <Stepper
            activeStep={activeStep}
            lineClassName="bg-white/50"
            activeLineClassName="bg-white">
            <Step
              className="h-4 w-4 !bg-blue-gray-50 text-white/75 cursor-pointer"
              activeClassName="ring-0 !bg-white text-white"
              completedClassName="!bg-white text-white"
              onClick={() => setActiveStep(0)}>
              <div className="absolute -bottom-[2.3rem] w-max text-center text-xs">
                <Typography variant="h6" color="inherit">
                  Face 1
                </Typography>
              </div>
            </Step>
            <Step
              className="h-4 w-4 !bg-blue-gray-50 text-white/75 cursor-pointer"
              activeClassName="ring-0 !bg-white text-white"
              completedClassName="!bg-white text-white"
              onClick={() => setActiveStep(1)}>
              <div className="absolute -bottom-[2.3rem] w-max text-center text-xs">
                <Typography variant="h6" color="inherit">
                  Face 2
                </Typography>
              </div>
            </Step>
            <Step
              className="h-4 w-4 !bg-blue-gray-50 text-white/75 cursor-pointer"
              activeClassName="ring-0 !bg-white text-white"
              completedClassName="!bg-white text-white"
              onClick={() => setActiveStep(2)}>
              <div className="absolute -bottom-[2.3rem] w-max text-center text-xs">
                <Typography variant="h6" color="inherit">
                  Face 3
                </Typography>
              </div>
            </Step>
            <Step
              className="h-4 w-4 !bg-blue-gray-50 text-white/75 cursor-pointer"
              activeClassName="ring-0 !bg-white text-white"
              completedClassName="!bg-white text-white"
              onClick={() => setActiveStep(3)}>
              <div className="absolute -bottom-[2.3rem] w-max text-center text-xs">
                <Typography variant="h6" color="inherit">
                  Face 4
                </Typography>
              </div>
            </Step>
          </Stepper>
        </div>
      </CardHeader>
      <CardBody>
        <div className="mb-2 flex items-center justify-center">
          <Typography color="blue-gray" className="font-medium">
            Capture Face
          </Typography>
        </div>
        <Typography
          variant="small"
          color="gray"
          className="font-normal opacity-75">
          Scan wajah sampai terdeteksi, jika sudah klik submit untuk registrasi
          data wajah
        </Typography>
      </CardBody>
      <CardFooter className="pt-0">
        {activeStep == 4 && (
          <Input
            label="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        )}
        <Button
          ref={btnCaptureRef}
          ripple={false}
          fullWidth={true}
          className="bg-blue-gray-900/10 text-blue-gray-900 shadow-none hover:scale-105 hover:shadow-none focus:scale-105 focus:shadow-none active:scale-100">
          {activeStep < 4 ? "CAPTURE" : "SUBMIT"}
        </Button>
      </CardFooter>
    </Card>
  );
};

export const Component = Register;
