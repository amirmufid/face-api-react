import { Typography } from "@material-tailwind/react";
const Home = () => {
  return (
    <>
      <Typography variant="h1" color="blue-gray">
        Material Tailwind React
      </Typography>
      <Typography variant="lead" color="blue-gray" className="opacity-70">
        An easy to use components library for Tailwind CSS and React.
      </Typography>
    </>
  );
};

export const Component = Home;
